package eu.wiktordolecki.maze.cli;

import eu.wiktordolecki.maze.*;
import org.jfree.svg.SVGUtils;
import picocli.CommandLine;
import picocli.CommandLine.ParameterException;

import java.io.File;
import java.io.IOException;
import java.util.Random;

public class Main {

    public static void main(String[] args) throws IOException {
        var request = new MazeRequest();
        var cmd = new CommandLine(request)
                .setCaseInsensitiveEnumValuesAllowed(true);

        try {
            cmd.parseArgs(args);
            if (cmd.isUsageHelpRequested()) {
                cmd.usage(System.out);
                return;
            }
        } catch (ParameterException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        var builder = new MazeBuilder(new Random());
        var renderer = new MazeRenderer();

        request.seed().ifPresent(builder::setSeed);
        request.tileSize().ifPresent(renderer::tileSize);

        var maze = builder.buildMaze(request.width(), request.height());
        var start = request.startPos().orElse(LocationPreference.ANY);
        var target = request.targetPos().orElse(LocationPreference.ANY);
        var mazeWithPath = builder.chartPath(maze, start, target);

        var svg = renderer.renderToSvg(mazeWithPath, request.showSolution());

        SVGUtils.writeToSVG(new File(request.outputFileName()), svg);
    }
}
