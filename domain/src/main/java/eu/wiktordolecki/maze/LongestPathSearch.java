package eu.wiktordolecki.maze;

import eu.wiktordolecki.maze.SquareMaze.SquareTile;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.*;
import java.util.function.Predicate;

public class LongestPathSearch {

    public static PriorityQueue<SearchPosition> longestPaths(SquareMaze maze, int startX, int startY) {
        SquareTile startTile = maze.at(startX, startY);

        Set<SquareTile> seen = new HashSet<>();
        Stack<SearchPosition> searchState = new Stack<>();

        seen.add(startTile);
        searchState.push(new SearchPosition(startTile, null, 0));

        PriorityQueue<SearchPosition> terminalPaths =
                new PriorityQueue<>(Comparator.comparing(SearchPosition::dist).reversed());

        while (!searchState.isEmpty()) {
            SearchPosition currPos = searchState.pop();

            List<SquareTile> toVisit = currPos.tile.neighbours()
                    .stream()
                    .filter(Predicate.not(seen::contains))
                    .toList();

            if (toVisit.isEmpty()) {
                terminalPaths.add(currPos);
            } else {
                toVisit.forEach(tile -> {
                    searchState.push(new SearchPosition(tile, currPos, currPos.dist + 1));
                    seen.add(tile);
                });
            }
        }
        return terminalPaths;
    }

    public record SearchPosition(SquareTile tile, LongestPathSearch.SearchPosition prev, int dist) {
        public List<SquareTile> path() {
            List<SquareTile> result = new ArrayList<>();
            SearchPosition curr = this;

            while (curr != null) {
                result.add(curr.tile);
                curr = curr.prev;
            }
            return result;
        }
    }
}
