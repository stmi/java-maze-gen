package eu.wiktordolecki.maze;

import eu.wiktordolecki.maze.SquareMaze.SquareTile;
import org.jfree.svg.SVGGraphics2D;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Point;
import java.util.Iterator;
import java.util.List;

public class MazeRenderer {

    public static final int DEFAULT_STEP = 40;

    private int tileSize;
    private int margin;
    private int rad;

    public MazeRenderer() {
        tileSize(DEFAULT_STEP);
    }

    public MazeRenderer tileSize(int tileSize) {
        this.tileSize = tileSize;
        this.margin = tileSize / 2 + 1;
        this.rad = tileSize / 4;

        return this;
    }

    public String renderToSvg(SquareMazeWithPath mazeWithPath, boolean showSolution) {
        SquareMaze maze = mazeWithPath.maze();
        List<SquareTile> path = mazeWithPath.path();

        SVGGraphics2D g2 = getSvgGraphics2D(maze);

        if (showSolution) {
            drawPath(path, g2);
        }

        g2.setColor(Color.RED);

        SquareTile startTile = mazeWithPath.start();
        Point startPoint = pointAtCell(startTile.x(), startTile.y());
        ovalAtPoint(g2, startPoint);

        SquareTile targetTile = mazeWithPath.target();
        Point targetPoint = pointAtCell(targetTile.x(), targetTile.y());
        ovalAtPoint(g2, targetPoint);


        drawVerticalWalls(maze, g2);
        drawHorizontalWalls(maze, g2);

        drawFrame(maze, g2);

        return g2.getSVGElement();
    }


    public String renderToSvg(SquareMaze maze) {
        SVGGraphics2D g2 = getSvgGraphics2D(maze);

        drawVerticalWalls(maze, g2);
        drawHorizontalWalls(maze, g2);

        drawFrame(maze, g2);

        return g2.getSVGElement();
    }

    private SVGGraphics2D getSvgGraphics2D(SquareMaze maze) {
        int graphicsWidth = maze.width() * tileSize + 2 * margin;
        int graphicsHeight = maze.height() * tileSize + 2 * margin;

        SVGGraphics2D g2 = new SVGGraphics2D(graphicsWidth, graphicsHeight);

        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, graphicsWidth, graphicsHeight);
        g2.setStroke(new BasicStroke(tileSize / 8));
        return g2;
    }

    private void drawPath(List<SquareTile> path, SVGGraphics2D g2) {
        g2.setPaint(Color.BLUE);

        if (path != null && path.size() > 1) {
            Iterator<SquareTile> iter = path.iterator();

            SquareTile curr = iter.next();
            while (iter.hasNext()) {
                SquareTile next = iter.next();

                Point currPoint = pointAtCell(curr.x(), curr.y());
                ovalAtPoint(g2, currPoint);

                Point nextPoint = pointAtCell(next.x(), next.y());
                ovalAtPoint(g2, nextPoint);

                g2.drawLine(currPoint.x, currPoint.y, nextPoint.x, nextPoint.y);

                curr = next;
            }
        }

    }

    private void drawFrame(SquareMaze maze, SVGGraphics2D g2) {
        g2.setPaint(Color.BLACK);

        int xStart = margin;
        int yStart = margin;

        int xEnd = margin;
        int yEnd = margin + maze.height() * tileSize;

        g2.drawLine(xStart, yStart, xEnd, yEnd);

        xStart = margin + maze.width() * tileSize;
        xEnd = margin + maze.width() * tileSize;

        g2.drawLine(xStart, yStart, xEnd, yEnd);

        xStart = margin;
        xEnd = margin + maze.width() * tileSize;

        yStart = margin;
        yEnd = margin;

        g2.drawLine(xStart, yStart, xEnd, yEnd);

        xStart = margin;
        xEnd = margin + maze.width() * tileSize;

        yStart = margin + maze.height() * tileSize;
        yEnd = margin + maze.height() * tileSize;

        g2.drawLine(xStart, yStart, xEnd, yEnd);
    }

    private void drawHorizontalWalls(SquareMaze maze, SVGGraphics2D g2) {
        g2.setPaint(Color.BLACK);

        for (int yPos = 1; yPos < maze.height(); yPos++) {
            int wallStart = 0;
            int wallEnd = 0;

            for (int xPos = 0; xPos < maze.width(); xPos++) {
                SquareTile upTile = maze.at(xPos, yPos - 1);
                SquareTile downTile = maze.at(xPos, yPos);

                if (upTile.isConnected(downTile)) {
                    if (wallStart == wallEnd) {
                        wallEnd++;
                        wallStart = wallEnd;
                    } else {
                        int xStart = margin + wallStart * tileSize;
                        int yStart = margin + yPos * tileSize;

                        int xEnd = margin + wallEnd * tileSize;

                        g2.drawLine(xStart, yStart, xEnd, yStart);

                        wallEnd++;
                        wallStart = wallEnd;
                    }
                } else {
                    wallEnd++;
                }
            }

            if (wallStart != wallEnd) {
                int xStart = margin + wallStart * tileSize;
                int yStart = margin + yPos * tileSize;

                int xEnd = margin + wallEnd * tileSize;

                g2.drawLine(xStart, yStart, xEnd, yStart);
            }
        }
    }

    private void drawConnections(SquareMaze maze, SVGGraphics2D g2) {
        g2.setPaint(Color.GRAY);

        for (int xPos = 0; xPos < maze.width(); xPos++) {
            for (int yPos = 0; yPos < maze.height(); yPos++) {
                SquareTile tile = maze.at(xPos, yPos);

                Point point = pointAtCell(xPos, yPos);

                ovalAtPoint(g2, point);

                if (tile.isConnected(tile.down())) {
                    g2.drawLine(point.x, point.y, point.x, point.y + tileSize);
                }

                if (tile.isConnected(tile.right())) {
                    g2.drawLine(point.x, point.y, point.x + tileSize, point.y);
                }
            }
        }
    }

    private void drawVerticalWalls(SquareMaze maze, SVGGraphics2D g2) {
        g2.setPaint(Color.BLACK);

        for (int xPos = 1; xPos < maze.width(); xPos++) {
            int wallStart = 0;
            int wallEnd = 0;

            for (int yPos = 0; yPos < maze.height(); yPos++) {
                SquareTile leftTile = maze.at(xPos - 1, yPos);
                SquareTile rightTile = maze.at(xPos, yPos);

                if (leftTile.isConnected(rightTile)) {
                    if (wallStart == wallEnd) {
                        wallEnd++;
                        wallStart = wallEnd;
                    } else {
                        int xStart = margin + xPos * tileSize;
                        int yStart = margin + wallStart * tileSize;

                        int yEnd = margin + wallEnd * tileSize;

                        g2.drawLine(xStart, yStart, xStart, yEnd);

                        wallEnd++;
                        wallStart = wallEnd;
                    }
                } else {
                    wallEnd++;
                }
            }

            if (wallStart != wallEnd) {
                int xStart = margin + xPos * tileSize;
                int yStart = margin + wallStart * tileSize;

                int yEnd = margin + wallEnd * tileSize;

                g2.drawLine(xStart, yStart, xStart, yEnd);
            }
        }
    }

    private void ovalAtPoint(SVGGraphics2D g2, Point start) {
        g2.fillOval(start.x - rad, start.y - rad, rad * 2, rad * 2);
    }

    private Point pointAtCell(int x, int y) {
        int xOut = 2 * margin + x * tileSize;
        int yOut = 2 * margin + y * tileSize;
        return new Point(xOut, yOut);
    }
}
