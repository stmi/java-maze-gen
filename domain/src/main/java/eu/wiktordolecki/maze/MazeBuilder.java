package eu.wiktordolecki.maze;

import eu.wiktordolecki.maze.SquareMaze.SquareTile;
import lombok.RequiredArgsConstructor;

import java.util.*;
import java.util.function.Predicate;

@RequiredArgsConstructor
public class MazeBuilder {

    private final Random random;

    public SquareMaze buildMaze(int width, int height) {
        SquareMaze maze = new SquareMaze(width, height);

        Set<SquareTile> seen = new HashSet<>();
        RandomizedQueue<SquareTile> toVisit = new RandomizedQueue<>(random);
        Set<SquareTile> visited = new HashSet<>();

        SquareTile start = maze.at(random.nextInt(width), random.nextInt(height));

        seen.add(start);
        toVisit.enqueue(start);

        while (!toVisit.isEmpty()) {
            SquareTile curr = toVisit.dequeue();
            visited.add(curr);

            curr.aroundStream()
                    .filter(Predicate.not(seen::contains))
                    .forEach(node -> {
                        toVisit.enqueue(node);
                        seen.add(node);
                    });

            List<SquareTile> visitedNeighbours = curr
                    .aroundStream()
                    .filter(visited::contains)
                    .toList();

            int size = visitedNeighbours.size();
            if (size > 0) {
                int idx = random.nextInt(size);
                curr.connect(visitedNeighbours.get(idx));
            }
        }

        return maze;
    }

    public SquareMazeWithPath chartPath(SquareMaze maze, LocationPreference start, LocationPreference target) {
        Predicate<LongestPathSearch.SearchPosition> startPredicate = switch (start) {
            case ANY -> pos -> true;
            case BORDER -> pos -> pos.tile().isBorder();
        };

        Predicate<LongestPathSearch.SearchPosition> targetPredicate = switch (target) {
            case ANY -> pos -> true;
            case BORDER -> pos -> pos.tile().isBorder();
        };

        PriorityQueue<LongestPathSearch.SearchPosition> paths = LongestPathSearch.longestPaths(maze, maze.width() / 2, maze.height() / 2);

        LongestPathSearch.SearchPosition position = paths.stream()
                .filter(startPredicate)
                .findFirst()
                .orElseThrow(NoSuchElementException::new);

        SquareTile tile = position.tile();

        List<SquareTile> path = LongestPathSearch
                .longestPaths(maze, tile.x(), tile.y())
                .stream()
                .filter(targetPredicate)
                .findFirst()
                .orElseThrow(NoSuchElementException::new)
                .path();

        return new SquareMazeWithPath(maze, path.get(0), path.get(path.size() -1), path);
    }

    public void setSeed(long seed) {
        random.setSeed(seed);
    }
}
