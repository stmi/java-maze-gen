package eu.wiktordolecki.maze;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.*;
import java.util.stream.Stream;

public class SquareMaze {

    @Getter
    private final int width;
    @Getter
    private final int height;

    private final SquareTile[][] tiles;

    public SquareMaze(int width, int height) {
        if (width <= 0) {
            throw new IllegalArgumentException("SquareGrid width has to be positive integer");
        }
        if (height <= 0) {
            throw new IllegalArgumentException("SquareGrid height has to be positive integer");
        }
        this.width = width;
        this.height = height;
        this.tiles = new SquareTile[width][height];

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                tiles[x][y] = new SquareTile(x, y, x * width + y);
            }
        }
    }

    public SquareTile at(int x, int y) {
        return tiles[x][y];
    }

    @Override
    public String toString() {
        return "SquareGrid{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }

    @Getter
    @RequiredArgsConstructor
    @ToString(exclude = {"parent", "neighbours"})
    public class SquareTile {
        private final int x;
        private final int y;
        private final int id;
        private final SquareMaze parent = SquareMaze.this;

        private final Set<SquareTile> neighbours = new HashSet<>();

        public void connect(SquareTile other) {
            if (other == null || this == other || this.parent != other.parent) {
                return;
            }

            this.neighbours.add(other);
            other.neighbours.add(this);
        }

        public boolean isConnected(SquareTile other) {
            if (other == null) {
                return false;
            } else if (this == other) {
                return false;
            } else if (this.parent != other.parent) {
                return false;
            }

            return this.neighbours.contains(other);
        }

        public SquareTile up() {
            return (y == 0) ? null : tiles[x][y - 1];
        }

        public SquareTile down() {
            return (y == height - 1) ? null : tiles[x][y + 1];
        }

        public SquareTile left() {
            return (x == 0) ? null : tiles[x - 1][y];
        }

        public SquareTile right() {
            return (x == width - 1) ? null : tiles[x + 1][y];
        }

        public Stream<SquareTile> aroundStream() {
            return Stream.of(up(), down(), left(), right())
                    .filter(Objects::nonNull);
        }

        public boolean isBorder() {
            boolean isTopBorder = y == 0;
            boolean isBottomBorder = y == height - 1;
            boolean isLeftBorder = x == 0;
            boolean isRightBorder = x == width - 1;

            return isTopBorder || isBottomBorder || isLeftBorder || isRightBorder;
        }
    }
}
