package eu.wiktordolecki.maze;

import eu.wiktordolecki.maze.SquareMaze.SquareTile;

import java.util.List;

public record SquareMazeWithPath(SquareMaze maze, SquareTile start, SquareTile target, List<SquareTile> path) {

}
