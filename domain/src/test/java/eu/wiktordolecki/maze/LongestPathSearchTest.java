package eu.wiktordolecki.maze;

import net.jqwik.api.Example;

import java.util.PriorityQueue;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

class LongestPathSearchTest {

    @Example
    void name() {
//        MazeBuilder builder = new MazeBuilder(new Random(87654));
        MazeBuilder builder = new MazeBuilder(new Random());

        SquareMaze maze = builder.buildMaze(10 , 10);

        PriorityQueue<LongestPathSearch.SearchPosition> paths = LongestPathSearch.longestPaths(maze, maze.width() / 2, maze.height() / 2);

        paths.forEach(System.out::println);

//        assertThat(paths.size()).isGreaterThanOrEqualTo(18);
//
//        List<LongestPathSearch.SearchPosition> list = Stream.generate(paths::poll)
//                .limit(paths.size())
//                .filter(pos -> pos.tile().isBorder())
//                .toList();
//
//        SquareMaze.SquareTile newStart = list.get(2).tile();
//
//        PriorityQueue<LongestPathSearch.SearchPosition> koniec = LongestPathSearch.longestPaths(maze);
//
//        LongestPathSearch.SearchPosition poll = koniec.poll();
//
//        System.out.println(newStart);
//        System.out.println(poll);
    }
}